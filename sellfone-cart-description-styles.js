import { css } from 'lit-element';

export default css`
:host {
  display: inline-block;
  box-sizing: border-box;
  width:100%;
  font-family: 'Roboto', sans-serif;
}

:host([hidden]), [hidden] {
  display: none !important;
}

*, *:before, *:after {
  box-sizing: inherit;
  font-family: inherit;
}

.descriptionContainer{
  width:100%;
}

.gridContainer{
  width:100%;
  display:grid;
  grid-template-columns: 5% 10% 55% 10% 10% 10%;
  align-items: center;
  border-bottom: 1px solid #efefef;
}

.item{
 width: 100%;

}

.removeIcon{
  height: 50px;
  cursor: pointer;
}

.productImage{
  height: 70px;
}

.priceContainer{
  text-align: center;
}

.imageContainer{
  text-align: center;
  margin: 20px 0;
}

.header{
  color: black;
  font-size: 125%;
}

.price{
  color: black;
}

.discountPrice{
  font-weight: bolder;
}

.responsiveDescription{
  display: none;
  border-bottom: 1px solid #efefef;
}

.responsiveDelete{
  color: black;
  text-align: right;
}

#deleteButton{
  cursor: pointer;
}

.responsiveContainer{
  display: none;
}

.visible{
  display: block;
}

.visibleResponsiveDescription{
  display: grid;
    grid-template-columns: 25% 60% 15%;
}

.responsiveItem{
  margin-top: 10px;
}

.responsiveImageContainerVisible{
  text-align: center;
}

.couponContainer{
  width: 100%;
  height: 40px;
  display: flex;
  flex-direction: row;
  margin-top: 20px;
}

.couponInput{
  flex-grow:2;
  font-size: 115%;
  border: 1px solid #efefef;
  outline: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.couponInput:focus, #couponButton{
  transition: 0.2s ease-in;
  border: 0.5px solid black;
 
  
}

#couponButton{
  background-color: var(--coupon-button-background,#009688);
  outline: none;
  font-weight: bold;
  color: white;
  cursor: pointer;
  transition: 0.2s;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  
}

#couponButton:hover{
  background-color: rgb(62, 62, 62);
}

.totalContainer{
  width: 100%;
  display: flex;
  flex-flow: row wrap;
}

.bottomContainer{
  display: grid;
  grid-template-columns: 50% 50%;
}

.totalInfoContainer{
  width: 100%;
  display:grid;
  grid-template-columns: 50% 50%;
  text-align: center;
}

.totalInfo{
  font-size: 130%;
  color: black;
}

.totalResponsiveContainer{
  width: 100%;
  display: grid;
  grid-template-columns: 50% 50%;
  text-align: center;
}

.buttons{
  
  padding:15px;
  font-size: 125%;
  color: white;
  font-weight: bold;
  outline: none;
  transition: 0.3s;
  border: none;
}

#cartButton{
  background-color:var(--cart-button-background,#009688) ;
  cursor: pointer;
}

#cartButton:hover{
  background-color: var(--cart-button-background-hover,#07ac9b) ;
}

#buyButton{
  background-color: var(--buy-button-background,#3F51B5);
  cursor: pointer;
}

#buyButton:hover{
  background-color: var(--buy-button-background-hover,rgb(74, 92, 190));
}

.itemName{
  text-decoration: none;
  border: 1px solid rgba(0,0,0,0);
  color: black;
  transition: 0.2s ease-in;
}

.itemName:hover{
  border-bottom: 1px solid rgba(0,0,0,1);
}

@media only screen and (max-width: 850px) {
  .gridContainer{
    grid-template-columns: 5% 10% 40% 15% 15% 15%;
    
  }
}

@media only screen and (max-width: 620px) {
  .gridContainer{
    grid-template-columns: 10% 15% 30% 15% 15% 15%;
    
  }
}

@media only screen and (max-width: 530px) {
  .descriptionContainer{
    display: none;
  }

  .responsiveContainer{
    display: block;
  }

  .responsiveDescription{
    display: grid;
    grid-template-columns: 25% 60% 15%;
    
  }
  .responsiveImageContainer{
    text-align: center;
  }
}

@media only screen and (max-width: 400px) {
  .productImage{
    max-width: 100%;
  }
}`;
