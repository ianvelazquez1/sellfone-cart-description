import { html, LitElement } from 'lit-element';
import style from './sellfone-cart-description-styles.js';

class SellfoneCartDescription extends LitElement {
  static get properties() {
    return {
      productArray:{
        type:Array,
        attribute:'product-array'
      },
      removeIconUrl:{
        type:String,
        attribute:'remove-icon-url'
      },
      couponName:{
        type:String,
        attribute:'coupon-name'
      },
      visibleTotal:{
        type:Boolean,
        attribute:'visible-total'
      },
      _total:{
        type:Object
      },
      mobileMode:{
        type:Boolean,
        attribute:'mobile-mode'
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.productArray=[];
    this.removeIconUrl="";
    this._total={
      subTotal:0,
      discountTotal:0,
      total:0
    };
    this.mobileMode=false;
    
  }

  

  render() {
    return html`
    ${this.mobileMode? html`
    <div class="responsiveContainer visible">
       ${this.__iterateOnlyMobileElements()}
       <div class="totalResponsiveContainer">
             <p class="totalInfo">Total</p>
             <p class="totalInfo">$${this._total.total}</p>
             <button class="buttons" id="cartButton" @click="${this.seeCart}">Cart</button>
             <button class="buttons" id="buyButton" @click="${this.seeBuy}">Buy</button>
       </div>`:
    html`<div class="descriptionContainer">
            <div class="gridContainer">
              <div class="item header"></div>
              <div class="imageContainer item header"></div>
              <div class="item header">Product</div>
              <div class="item header priceContainer">Price</div>
              <div class="item header priceContainer discountPrice">Discount</div>
              <div class="item header priceContainer">Final Price</div>
              </div>
              ${this._iterateItems()}
            <div class="bottomContainer">
            <div class="couponContainer">
            <input class="couponInput" id="couponInput" type="text" placeholder="coupon">
             <button id="couponButton" @click="${this.applyCoupon}">Apply coupon</button>
            </div>
            <div class="totalContainer">
                <div class="totalInfoContainer">
                    <p class="totalInfo">Subtotal</p>
                    <p class="totalInfo">$${this._total.subTotal}</p>
                </div>
                <div class="totalInfoContainer">
                    <p class="totalInfo">Discount</p>
                    <p class="totalInfo">$${this._total.discountTotal}</p>
                </div>
                <div class="totalInfoContainer">
                <p class="totalInfo">Total</p>
                <p class="totalInfo">$${this._total.total}</p>
            </div>
            </div>
            </div>
        </div>
      <div class="responsiveContainer">
       
        ${this._iterateResponsiveItems()}
        <div class="totalResponsiveContainer">
              <p class="totalInfo">Total</p>
              <p class="totalInfo">$${this._total.total}</p>
              <button class="buttons" id="cartButton" @click="${this.seeCart}">Cart</button>
              <button class="buttons" id="buyButton" @click="${this.seeBuy}">Buy</button>
        </div>
      </div>`}
        
      `;
    }

    _iterateItems(){
    return this.productArray.map((product,index)=>{
        return html`
        <div class="gridContainer">
            <div class="item" ><img src="${this.removeIconUrl}" class="removeIcon" @click="${(e)=>this.removeItem(index)}" alt="remove item icon"></div>
            <div class="imageContainer">
            ${product.images?html`<img src="${product.images[0]}" class="productImage">`:html`<img src="" class="productImage" alt="no image found">`}</div>
            <div class="item"><a href="#" class="itemName">${product.title}</a></div>
            <div class="item priceContainer"><p class="price">$${product.price}</p></div>
            <div class="item priceContainer"><p class="price discountPrice">$${product.price-product.discountedPrice}</p></div>
            <div class="item priceContainer"><p class="price finalPrice">$${product.discountedPrice}</p></div>
        </div>
        
        `
      })
    }

    _iterateResponsiveItems(){
      return this.productArray.map((product,index)=>{
        return html`
        <div class="responsiveDescription">
          <div class="responsiveItem responsiveImageContainer"><img class="productImage" src="${product.images[0]}"></div>
          <div class="responsiveItem middleItem"><p class="itemName">${product.title}</p><p>1x <span>${product.discountedPrice}</span></p></div>
          <div class="responsiveItem responsiveDelete"><p id="deleteButton" @click="${(e)=>this.removeItem(index)}">X</p></div>
        </div>
        `
      });
    }

    __iterateOnlyMobileElements(){
      return this.productArray.map((product,index)=>{
        return html`
        <div class="responsiveDescription visibleResponsiveDescription">
          <div class="responsiveItem responsiveImageContainerVisible"><img class="productImage" src="${product.images[0]}"></div>
          <div class="responsiveItem middleItem"><p class="itemName">${product.title}</p><p>1x <span>${product.discountedPrice}</span></p></div>
          <div class="responsiveItem responsiveDelete"><p id="deleteButton" @click="${(e)=>this.removeItem(index)}">X</p></div>
        </div>
        `
      });
    }

    removeItem(event){
        this._updateTotal(event);
        this.productArray.splice(event,1);
        this.update();
        this.dispatchEvent(new CustomEvent("item-removed",{
          bubbles:false,
          composed:false,
          detail:this.productArray[event]
        }));
    }

    _updateTotal(index){
        console.log(index);
        this._total.subTotal-=this.productArray[index].price;
        this._total.discountTotal-=this.productArray[index].price-this.productArray[index].discountedPrice;
        this._total.total-=this.productArray[index].discountedPrice;
    }

    applyCoupon(){
     const coupon=this.shadowRoot.querySelector("#couponInput").value;
     this.dispatchEvent(new CustomEvent("coupon-applied",{
       bubbles:false,
       composed:false,
       detail:coupon
     }));
     
    }

    updated(updatedProperties){
      if(updatedProperties.has("productArray")){
      this._getTotal();
      this.update();
      }
    }

    _getTotal(){
      for(const product of this.productArray){
        this._total.subTotal=this._total.subTotal+product.price;
        this._total.discountTotal+=product.price-product.discountedPrice;
        this._total.total+=product.discountedPrice;
      }
     
    }

    seeCart(){
      this.dispatchEvent(new CustomEvent('see-cart',{
        bubbles:false,
        composed:false,
        detail:"see cart"
      }));
    }

    seeBuy(){
      this.dispatchEvent(new CustomEvent('see-buy',{
        bubbles:false,
        composed:false,
        detail:"see buy"
      }));
    }
}

window.customElements.define("sellfone-cart-description", SellfoneCartDescription);
