/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-cart-description.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-cart-description></sellfone-cart-description>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
